package sheridan;

public class Celsius {
	public static int fromFahrenheit(int temp) {
		if(temp<0) {
			return 0;
		}		
		return (int) Math.round(( 5 *(temp - 32.0)) / 9.0);
	}
	
	
}

package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author Rishabh Bajaj
 *
 */

public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		int celsius = Celsius.fromFahrenheit(5);
		assertTrue("Invalid conversion from f to c", celsius==-15);
	}
	
	
	@Test
	public void testFromFahrenheitException() {
		int celsius = Celsius.fromFahrenheit(-20);
		assertFalse("Invalid conversion from f to c", celsius==-29);
	}
	
	@Test
	public void testFromFahrenheitBoundryIn() {
		int celsius = Celsius.fromFahrenheit(0);
		assertTrue("Invalid conversion from f to c", celsius==-18);
	}
	
	@Test
	public void testFromFahrenheitBoundryOut() {
		int celsius = Celsius.fromFahrenheit(-1);
		assertFalse("Invalid conversion from f to c", celsius==-18);
	}
}
